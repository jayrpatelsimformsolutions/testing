var chai = require('chai')
var assert = chai.assert;
var should = chai.should();
var expect = chai.expect;

//assert

describe('Aspect check', function () {
    let username = 'jay';
    let mylist = {
        items: [{
                id: 1,
                name: "demo"
            }, {
                id: 2,
                name: "demo2"
            }

        ],
        title: 'hbejue'
    }
    it('check type', function () {
        assert.typeOf(username, 'string');
    });

    it('equal match', function () {
        assert.equal(username, 'jay');
    })

    it('check length', function () {
        assert.lengthOf(mylist.items, 2)
    })
})


// should

describe("check using should", function () {
    let username = 'jay';
    let mylist = {
        items: [{
                id: 1,
                name: "demo"
            }, {
                id: 2,
                name: "demo2"
            }

        ],
        title: 'hbejue'
    }
    it('check string', function () {
        username.should.be.a("string");
    })

    it('equal match', function () {
        username.should.equal('jay')
    })

    it('check length', function () {
        mylist.should.have.property('items').with.lengthOf(2);
    })


})

//expect

describe("expect check", function () {
    let username = 'jay';
    let mylist = {
        items: [{
                id: 1,
                name: "demo"
            }, {
                id: 2,
                name: "demo2"
            }

        ],
        title: 'hbejue'
    }
    it("string check", function () {
        expect(username).to.be.a('string');
    })

    it("equal string", function () {
        expect(username).to.equal('jay');

    })
    it("check length", function () {
        expect(mylist).have.property('items').with.lengthOf(2);
    })
})