var assert = require ('assert')
describe('Array',function(){
    describe('#indexOf()',function(){
        it('should return -1 when the value is not present',function(){
            assert.equal([1,2,3].indexOf(3),2)
        });
    });
});


describe("first test case",function(){
    it("check value",function(){
        assert.equal([2,3,4].indexOf(3),1);
    })

    it("check value check 2",function(){
        assert.equal([2,3,4].indexOf(2),0);
    })
})

describe("second testcase",function(){
    it("give sum validation",function(){
        assert.equal(2+3,5);
    })
})